package com.nhom6.buser;

public class CoffeeObject {
    Double lat;
    Double lng;
    String name;
    String address;
    Double distance;
    Integer wishlist;

    public CoffeeObject(double lat, double lng, String name, String address, double distance, int wishlist) {
        this.lat = lat;
        this.lng = lng;
        this.name = name;
        this.address = address;
        this.distance = distance;
        this.wishlist = wishlist;
    }
}
