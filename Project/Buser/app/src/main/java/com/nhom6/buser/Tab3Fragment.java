package com.nhom6.buser;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Tab3Fragment extends Fragment implements CallBackClickItemListView {

    int duration = Toast.LENGTH_SHORT;
    List<CoffeeObject> wishlistCollection = new ArrayList<CoffeeObject>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);

        View v = inflater.inflate(R.layout.fragment_tab3, container, false);

        ListView listResult = v.findViewById(R.id.listViewWishlistFrag3);
        listResult.setAdapter(null);

        this.wishlistCollection = ((CoffeeApplication) getActivity().getApplication()).getWishlistCollection();
        Log.v("testwishlist", String.valueOf(this.wishlistCollection.size()));

        if (this.wishlistCollection != null) {
            List<CoffeeObject> collectionCoffee = new ArrayList<CoffeeObject>();


            for (int i = 0; i < this.wishlistCollection.size(); i++) {

                CoffeeObject coffee = this.wishlistCollection.get(i);
                collectionCoffee.add(coffee);
            }

            CustomAdapter customAdapter = new CustomAdapter(getActivity(), collectionCoffee, this);

            listResult.setAdapter(customAdapter);
        } else {
            Toast toast = Toast.makeText(getContext(), "Danh sách yêu thích đang trống", this.duration);
            toast.show();
        }


        return v;
    }


    @Override
    public void onResume() {
//        this.wishlistCollection = ((CoffeeApplication) getActivity().getApplication()).getWishlistCollection();
//
//        ListView listResult = getView().findViewById(R.id.listViewWishlistFrag3);
//        listResult.setAdapter(null);

        super.onResume();
    }


    @Override
    public void doListViewItemClickListerner(CoffeeObject coffee) {

    }
}
