package com.nhom6.buser;

import android.app.Application;

import java.util.List;

public class CoffeeApplication extends Application {
    private static boolean activityVisible;

    private List<CoffeeObject> wishlistCollection;

    public List<CoffeeObject> getWishlistCollection() {
        return wishlistCollection;
    }

    public void setWishlistCollection(List<CoffeeObject> coffeeCollection) {
        this.wishlistCollection = coffeeCollection;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static CoffeeApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized CoffeeApplication getInstance() {
        return mInstance;
    }

    public void setConnectionListener(ConnectionReceiver.ConnectionReceiverListener listener) {
        ConnectionReceiver.connectionReceiverListener = listener;
    }
}
