package com.nhom6.buser;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class CustomAdapter extends BaseAdapter {
    Context context;
    List<CoffeeObject> collection = new ArrayList<CoffeeObject>();
    LayoutInflater inflter;
    List<CoffeeObject> wishlistCollection = new ArrayList<CoffeeObject>();
    int duration = Toast.LENGTH_SHORT;

    CallBackClickItemListView CallBackClickItemListView;


    public CustomAdapter(Context applicationContext, List<CoffeeObject> collection, CallBackClickItemListView CallBackClickItemListView) {
        this.context = applicationContext;
        this.collection = collection;
        inflter = (LayoutInflater.from(applicationContext));

        this.CallBackClickItemListView = CallBackClickItemListView;
    }

    @Override
    public int getCount() {
        return collection.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.activity_list_view, null);
        TextView txtViewName = (TextView) view.findViewById(R.id.txtViewName);
        TextView txtViewAddress = (TextView) view.findViewById(R.id.txtViewAddress);
        TextView txtDistance = (TextView) view.findViewById(R.id.txtDistance);
        txtViewName.setText(collection.get(i).name);
        txtViewAddress.setText(collection.get(i).address);
        txtDistance.setText(collection.get(i).distance.toString() + "Km");

        final ImageButton wishlistBtn = view.findViewById(R.id.imgBtnWishListFrag1);
        wishlistBtn.setTag(collection.get(i));
        wishlistBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                initWishlistCollection();

                int status = onCLickBtnWishList(wishlistBtn);
                CoffeeObject coffee = collection.get(i);
                coffee.wishlist = status;
                collection.set(i, coffee);

                setWishlistCollection();
            }
        });

        if (collection.get(i).wishlist == 1) {
            wishlistBtn.setImageResource(R.drawable.star);
        } else {
            wishlistBtn.setImageResource(R.drawable.wishlist_button);
        }

        final ImageView logoBtn = view.findViewById(R.id.iconListItem);
        logoBtn.setTag(collection.get(i));

        final CoffeeObject coffeeSelected = collection.get(i);

        logoBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                callCallBack(coffeeSelected);
            }
        });


        return view;
    }


    private void callCallBack(CoffeeObject coffee) {
        this.CallBackClickItemListView.doListViewItemClickListerner(coffee);
    }


    private void initWishlistCollection() {
        Activity ac = (Activity) this.context;
        List<CoffeeObject> collection = ((CoffeeApplication) ac.getApplication()).getWishlistCollection();
        if (collection == null) {
            this.wishlistCollection = new ArrayList<CoffeeObject>();
        } else {
            this.wishlistCollection = collection;
        }
    }

    private void setWishlistCollection() {
        Activity ac = (Activity) this.context;
        ((CoffeeApplication) ac.getApplication()).setWishlistCollection(this.wishlistCollection);
    }

    private void addItemIntoWishlistCollection(CoffeeObject coffee) {
        if (this.wishlistCollection != null) {
            boolean flag = false;
            for (int i = 0; i < this.wishlistCollection.size(); i++) {
                if (this.wishlistCollection.get(i).lat == coffee.lat && this.wishlistCollection.get(i).lng == coffee.lng) {
                    flag = true;
                }
            }
            if (flag == false) {
                this.wishlistCollection.add(coffee);
            }
        } else {
            this.wishlistCollection.add(coffee);
        }

    }

    private void removeItemOutWishlistCollection(CoffeeObject coffee) {
        for (int i = 0; i < this.wishlistCollection.size(); i++) {
            Log.v("testwishlist", "Khi ấn nút wishlist và quét for remove, object coffee : " + " " +  coffee.name.toString() + " " + coffee.lat.toString() + " " + coffee.lng.toString() );
            Log.v("testwishlist", "Khi ấn nút wishlist và quét for remove : wishlist (i) : " + " " + this.wishlistCollection.get(i).name.toString() + " " + this.wishlistCollection.get(i).lat.toString() + " " + this.wishlistCollection.get(i).lng.toString() );
            if (this.wishlistCollection.get(i).lat.doubleValue() == coffee.lat.doubleValue() && this.wishlistCollection.get(i).lng.doubleValue() == coffee.lng.doubleValue()) {
                Log.v("testwishlist", "Khi ấn nút wishlist và xóa thành công : " + String.valueOf(this.wishlistCollection.size()));
                this.wishlistCollection.remove(i);
            }
        }

        this.setWishlistCollection();
    }



    private int onCLickBtnWishList(ImageButton imgBtn)
    {
        initWishlistCollection();
        Log.v("testwishlist", "Khi ấn nút wishlist : " + String.valueOf(this.wishlistCollection.size()));

        if (imgBtn.getDrawable().getConstantState() == this.context.getResources().getDrawable( R.drawable.wishlist_button).getConstantState())
        {

            CoffeeObject coffeeObject = (CoffeeObject) imgBtn.getTag();

            imgBtn.setImageResource(R.drawable.star);
            Toast toast = Toast.makeText(context, "Đã lưu vào mục yêu thích", duration);
            toast.show();

            addItemIntoWishlistCollection(coffeeObject);

            return 1;
        }
        else
        {
            CoffeeObject coffeeObject = (CoffeeObject) imgBtn.getTag();

            imgBtn.setImageResource(R.drawable.wishlist_button);
            Toast toast = Toast.makeText(context, "Đã xóa khỏi mục yêu thích", duration);
            toast.show();

            removeItemOutWishlistCollection(coffeeObject);

            return 0;
        }
    }
}