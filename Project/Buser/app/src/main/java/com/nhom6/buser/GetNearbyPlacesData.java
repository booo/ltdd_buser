package com.nhom6.buser;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;

import java.util.HashMap;
import java.util.List;

public class GetNearbyPlacesData extends AsyncTask<Object, String, String> {

    String googlePlacesData;
    GoogleMap mMap;
    String url;
    public List<HashMap<String, String>> dataResult = null;
    CallBackNearbyCafe CallBackNearbyCafe;


    public GetNearbyPlacesData(CallBackNearbyCafe CallBackNearbyCafe) {
        this.CallBackNearbyCafe = CallBackNearbyCafe;
    }

    @Override
    protected String doInBackground(Object... params) {
        try {
            Log.d("GetNearbyPlacesData", "doInBackground entered");
            mMap = (GoogleMap) params[0];
            url = (String) params[1];
            DownloadUrl downloadUrl = new DownloadUrl();
            googlePlacesData = downloadUrl.readUrl(url);
            Log.d("GooglePlacesReadTask", "doInBackground Exit");
        } catch (Exception e) {
            Log.d("GooglePlacesReadTask", e.toString());
        }
        return googlePlacesData;
    }

    @Override
    protected void onPostExecute(String result) {
//        Log.d("GooglePlacesReadTask", "onPostExecute Entered");
//        List<HashMap<String, String>> nearbyPlacesList = null;
//        DataParser dataParser = new DataParser();
//        nearbyPlacesList =  dataParser.parse(result);

        this.CallBackNearbyCafe.doGetNearByCafeFinished(result);

//        this.dataResult = nearbyPlacesList;

//        ShowNearbyPlaces(nearbyPlacesList);

//        Log.v("myTag", nearbyPlacesList.toString());
//        Log.d("GooglePlacesReadTask", "onPostExecute Exit");
    }





    public List<HashMap<String, String>> getData()
    {
        return this.dataResult;
    }

}
