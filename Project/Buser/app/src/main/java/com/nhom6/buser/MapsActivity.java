package com.nhom6.buser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;


import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


public class MapsActivity extends FragmentActivity implements ConnectionReceiver.ConnectionReceiverListener {

    private static final String PREFS_TAG = "SharedPrefs";
    private static final String WISHLIST_TAG = "Wishlist";

    TabLayout tabLayout;
    ViewPager viewPager;
    PageAdapter pageAdapter;
    TabItem tab1;
    TabItem tab2;
    TabItem tab3;
    TabItem tab4;

    String wishlistCollectionFileName = "coffee_collection.txt";

    private List<CoffeeObject> wishlistCollection = new ArrayList<CoffeeObject>();




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        if (checkConnection()) {

            initActivity();
        } else {
            Toast.makeText(this, "Vui lòng bật internet để sử dụng tính năng bản đồ !",Toast.LENGTH_SHORT).show();
        }

    }


    private void initActivity() {

        //Load Wishlist Collection from internal file, then save to global variable
        this.getWishlistFromSharedPreferences();
        //get global variable
//        ((MyApplication) this.getApplication()).getSomeVariable();


        //PageViewer
        tabLayout = findViewById(R.id.tabMainLayout);
        tab1 = findViewById(R.id.tabHome);
        tab2 = findViewById(R.id.tabSearch);
        tab3 = findViewById(R.id.tabWishList);
        tab4 = findViewById(R.id.tabAccount);
        viewPager = findViewById(R.id.viewPaper);

        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                Log.v("myTagPosition", String.valueOf(tab.getPosition()).toString());
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 2) {
                    setWishlistFromSharedPreferences();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1) {

                }
//                if (tab.getPosition() == 2) {
//                    CustomAdapter customAdapter = new CustomAdapter( getApplicationContext(), getWisthlistCollection());
//                    ListView listResult = findViewById(R.id.listViewWishlistFrag3);
//                    listResult.setAdapter(null);
//                    listResult.setAdapter(customAdapter);
//                }
                setWishlistFromSharedPreferences();
            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if(!isConnected) {

            //show a No Internet Alert or
            Toast.makeText(this, "Vui lòng kiểm tra lại internet !",Toast.LENGTH_SHORT).show();

        }else{

            // dismiss the dialog or refresh the activity
        }
    }


    public void onClickListenerIconListViewItem()
    {

    }



    @Override
    protected void onResume() {
        super.onResume();
        CoffeeApplication.activityResumed();

        CoffeeApplication.getInstance().setConnectionListener(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        CoffeeApplication.activityPaused();
        setWishlistFromSharedPreferences();
    }


    private boolean checkConnection() {
        boolean isConnected = ConnectionReceiver.isConnected();
        if(!isConnected) {
            return false;
        }

        return true;
    }



    public List<CoffeeObject> getWisthlistCollection() {
        this.wishlistCollection = ((CoffeeApplication) getApplication()).getWishlistCollection();
        return this.wishlistCollection;
    }


    public boolean fileExist(String fname){
        File file = getBaseContext().getFileStreamPath(fname);
        return file.exists();
    }


    public void loadInternalWishlistCollection() {
        if (!fileExist(this.wishlistCollectionFileName)) {
            File file = new File(getApplicationContext().getFilesDir(), this.wishlistCollectionFileName);
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }



        List<CoffeeObject> coffeeCollection = new ArrayList<CoffeeObject>();
        List<CoffeeObject> wishlistCollection = new ArrayList<CoffeeObject>();
        FileInputStream inStream;
        try {
            inStream = getApplicationContext().openFileInput(this.wishlistCollectionFileName);

            ObjectInputStream objectInStream = new ObjectInputStream(inStream);

            coffeeCollection = ((List<CoffeeObject>) objectInStream.readObject());
            objectInStream.close();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        } catch (OptionalDataException e1) {
            e1.printStackTrace();
        } catch (StreamCorruptedException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        if( coffeeCollection != null ) {
            for (int i = 0; i < coffeeCollection.size(); i++) {
                coffeeCollection.set(i, wishlistCollection.get(i));
            }

            this.wishlistCollection = wishlistCollection;
        } else
        {
            Log.e("FAILED TO LOAD ", "Wishlist file has missed !");
        }
    }

    public void updateInternalCoffeeCollection() {
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(this.wishlistCollectionFileName, Context.MODE_PRIVATE);
            ObjectOutputStream os = new ObjectOutputStream(outputStream);

            os.writeObject(this.getWisthlistCollection());
            os.close();

            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void getWishlistFromSharedPreferences() {
        Gson gson = new Gson();
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        String jsonPreferences = sharedPref.getString(WISHLIST_TAG, "");

        Type type = new TypeToken<List<CoffeeObject>>() {}.getType();

        Log.v("myTag", jsonPreferences.toString());

        this.wishlistCollection = gson.fromJson(jsonPreferences, type);
        ((CoffeeApplication) getApplication()).setWishlistCollection(this.wishlistCollection);
    }

    private void setWishlistFromSharedPreferences() {
        Gson gson = new Gson();
        String jsonCurProduct = gson.toJson(this.getWisthlistCollection());

        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFS_TAG, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();

        Log.v("myTag", jsonCurProduct.toString());

        editor.putString(WISHLIST_TAG, jsonCurProduct);
        editor.commit();
    }



}







